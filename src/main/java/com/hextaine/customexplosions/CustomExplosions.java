package com.hextaine.customexplosions;
/*
 * Created by Hextaine on 8/14/2019.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import net.md_5.bungee.api.ChatColor;

import com.hextaine.customexplosions.listeners.Explosion;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class CustomExplosions extends JavaPlugin {
    private static Plugin instance;
    private Config config = null;

    @Override
    public void onEnable() {
        this.config = new Config(this, "config.yml");

        if (!getConfig().isSet("CreeperExplosionRadius"))
            getConfig().set("CreeperExplosionRadius", 4);

        this.instance = this;

        this.getServer().getPluginManager().registerEvents(new Explosion(), this);
    }

    @Override
    public void onDisable() {}

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (label.equalsIgnoreCase("customexplosions")) {
            if (args.length == 0) {
                // TODO show help
                sender.sendMessage(ChatColor.RED + "Boom!");
                return true;
            }
            if (args[0].equalsIgnoreCase("reload")) {
                if (!sender.hasPermission("customexplosions.reload")) {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permissions for this");
                    return true;
                }
                if (args.length > 1) {
                    sender.sendMessage(ChatColor.BLUE + "Usage: /customexplosions reload");
                    return true;
                }

                // TODO do the thing
                this.config.reload();
                return true;
            }
        }
        return false;
    }
}
