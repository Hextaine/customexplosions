package com.hextaine.customexplosions.listeners;
/*
 * Created by Hextaine on 5/8/2021.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import com.hextaine.customexplosions.utils.LocationUtils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.util.Vector;

import net.md_5.bungee.api.ChatColor;

public class Explosion implements Listener {
    // Master list shared between all instances to replace explosions from these entity types
    private static ArrayList<EntityType> replaceExplosionsFrom = new ArrayList<EntityType>();
    private static ArrayList<Material> forbiddenBlocks = new ArrayList<>();
    private static boolean hasLoaded = false;
    private static final List<Vector> unitVectors = Arrays.asList(new Vector(1,1,0), new Vector(1,1,1), new Vector(0,1,1), new Vector(0, 1,0), new Vector(-1,1,0), new Vector(-1,1,1), new Vector(0,1,-1));

    private static final double explosionPower = 4.0;

    @EventHandler
    public void onExplosion(EntityExplodeEvent e) {
        // Load in the config stuff if we haven't already
        if (!hasLoaded) loadConfigValues();

        if (e.isCancelled()) {
            return; // respect if someone doesn't want us to explode
        }

        // Check if we should be replacing this explosion
        if (!shouldReplaceExplosion(e.getEntityType())) {
            return;
        }

        // Stop the normal explosion from happening, but recreate the particles and sound so we can still
        // show the explosion but without and world destruction
        e.setCancelled(true);
        e.getLocation().getWorld().spawnParticle(Particle.EXPLOSION_LARGE, e.getLocation(), 1);
        e.getLocation().getWorld().playSound(e.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 100, 100);


        // Destroy the blocks we need to
        Hexplosion explosion = new Hexplosion(e.getLocation(), 4);
        explosion.calculateExplosion();

        // Get the blocks we want to explode outward
        
        ArrayList<Block> blocksToThrow = new ArrayList<>();

        double thrownBlockYield = 0.5; // TODO config get yield of blocks thrown
        int numBlocksThrown = (int)(explosion.getExplodedBlocks().size() * thrownBlockYield);
        Collections.shuffle(explosion.getExplodedBlocks());
        for (int i = 0; i < numBlocksThrown && i < explosion.getExplodedBlocks().size(); i++) {
            if (!forbiddenBlocks.contains(explosion.getExplodedBlocks().get(i).getType())) {
                blocksToThrow.add(explosion.getExplodedBlocks().get(i));
            } else {
                // pretend like this index doesn't exist
                numBlocksThrown++;
            }
        }

        // Spawn the blocks we want to throw
        ArrayList<FallingBlock> thrownBlocks = new ArrayList<>();
        for (int i = 0; i < blocksToThrow.size(); i++) {
            // Spawn blocks to throw
            thrownBlocks.add(e.getLocation().getWorld().spawnFallingBlock(e.getLocation(), blocksToThrow.get(i).getBlockData()));
        }

        // calculate where and how strongly to throw the blocks
        for (int i = 0; i < thrownBlocks.size(); i++) {
            thrownBlocks.get(i).setVelocity(unitVectors.get(i % unitVectors.size()));
        }

        double dropYield = 0.3;
        explosion.explodeBlocks(new BlockExplodeHandler() {
            public void explodeBlock(Block b) {
                if (Math.random() < dropYield) {
                    b.breakNaturally();
                }
                b.setType(Material.AIR);
            }
        });
    }
    
    private void loadConfigValues() {
        Collections.shuffle(unitVectors);

        // TODO config forbidden blocks
        hasLoaded = true;
    }

    private boolean shouldReplaceExplosion(EntityType ent) {
        // TODO config get list of replaced explosion types
        if (replaceExplosionsFrom.contains(ent)) {}

        return true;
    }
}
