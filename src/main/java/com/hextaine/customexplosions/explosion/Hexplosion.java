package com.hextaine.customexplosions.explosion;

import java.util.ArrayList;
import java.util.HashSet;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.util.Vector;

public class Hexplosion {
    private Location epicenter;
    private double power;
    private ArrayList<ExplosiveRay> explosiveRays;

    private HashSet<Location> explodedLocations;
    private ArrayList<Block> explodedBlocks;

    public Hexplosion(Location epicenter, double power) {
        this.epicenter = epicenter;
        this.power = power;

        this.explosiveRays = new ArrayList<>();
        this.explodedLocations = new HashSet<>();

        for (double xIdx = -0.5; xIdx <= 0.5; xIdx += 0.0625) {
            for (double yIdx = -0.5; yIdx <= 0.5; yIdx += 0.0625) {
                for (double zIdx = -0.5; zIdx <= 0.5; zIdx += 0.0625) {
                    Vector direction = new Vector(xIdx, yIdx, zIdx);
                    explosiveRays.add(new ExplosiveRay(power, epicenter, direction));
                }
            }
        }
    }

    public void calculateExplosion() {
        // Thread explosionThread = new Thread(new Runnable() {
            // public void run() {
                HashSet<ExplosiveRay> raysToRemove= new HashSet<>(); 

                while (explosiveRays.size() > 0) {
                    for (ExplosiveRay ray : explosiveRays) {
                        Location explodedLocation = ray.getIfExplodedLocation();
                        if (explodedLocation != null) {
                            explodedLocations.add(explodedLocation);
                        }

                        ray.incrementExplosion();
                        
                        if (!ray.isActive()) {
                            raysToRemove.add(ray);
                        }
                    }

                    for (ExplosiveRay rayToRemove : raysToRemove) {
                        explosiveRays.remove(rayToRemove);
                    }
                    raysToRemove.clear();
                }
        //     }
        // });

        // explosionThread.start();
        // try {
        //     explosionThread.join();
        // } catch (Exception e) {
        //     Bukkit.getServer().broadcastMessage(ChatColor.DARK_RED + "failed to explode: " + e.getMessage());
        //     e.printStackTrace();
        // }
    }

    public ArrayList<Block> getExplodedBlocks() {
        if (this.explodedBlocks == null) {
            ArrayList<Block> explodedBlocks = new ArrayList<>();
            for (Location explodedLoc : this.explodedLocations) {
                explodedBlocks.add(explodedLoc.getBlock());
            }
            this.explodedBlocks = explodedBlocks;
        }

        return this.explodedBlocks;
    }

    public void deleteExplodedBlocks() {
        for (Block explodedBlock : getExplodedBlocks()) {
            explodedBlock.setType(Material.AIR);
        }
    }

    
    public void explodeBlocks(BlockExplodeHandler explodeHandler) {
        for (Block explodedBlock : getExplodedBlocks()) {
            explodeHandler.explodeBlock(explodedBlock);
        }
        deleteExplodedBlocks();
    }
}

interface BlockExplodeHandler {
    public void explodeBlock(Block b);
}
