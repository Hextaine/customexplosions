package com.hextaine.customexplosions.explosion;

import com.hextaine.customexplosions.utils.LocationUtils;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class ExplosiveRay {
    private static final double stepLength = 0.3;

    private double powerRemaining;
    private Location location;
    private Location lastLocation;
    private Vector direction;

    public ExplosiveRay(double power, Location epicenter, Vector direction) {
        this.powerRemaining = power;
        this.location = epicenter;
        this.lastLocation = epicenter;
        this.direction = normalizeVector(direction);
    }

    public boolean isActive() {
        return powerRemaining > 0;
    }

    private static Vector normalizeVector(Vector vec) {
        if (vec.isNormalized()) {
            return vec;
        }

        double magnitude = Math.sqrt(Math.pow(vec.getX(), 2) + Math.pow(vec.getY(), 2) + Math.pow(vec.getZ(), 2));
        return vec.multiply(1.0 / magnitude);
    }

    public void incrementExplosion() {
        this.lastLocation = LocationUtils.newLocation(this.location);

        if (powerRemaining >= 0) {
            this.location.add(direction);
            drainPower();
        }
    }

    /**
     * Returns an exploded location if the last block we calculated should be exploded.
     * @return
     */
    public Location getIfExplodedLocation() {
        // Check if we've passed into a new block
        if (this.location.toVector().toBlockVector() != this.lastLocation.toVector().toBlockVector()) {
            return this.lastLocation;
        }
        return null;
    }

    /**
     * Decrement the power remaining by how much power it'd take to get to this point.
     */
    public void drainPower() {
        double requiredPowerForStep = (0.225 * stepLength * 0.75);
        requiredPowerForStep *= (this.location.getBlock().getType().getBlastResistance() + 0.3 * stepLength);

        this.powerRemaining -= requiredPowerForStep;
    }
}
