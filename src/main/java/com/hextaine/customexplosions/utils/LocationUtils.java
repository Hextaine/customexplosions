package com.hextaine.customexplosions.utils;

/*
 * Created by Hextaine on 5/12/2021.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class LocationUtils {

    public static double sqDistance(double x1, double x2, double y1, double y2, double z1, double z2) {
        return Math.pow(x1-x2,2) + Math.pow(y1 - y2, 2) + Math.pow(z1 - z2, 2);
    }

    public static boolean isLocationInRadius(Location loc1, Location loc2, double radius) {
        return sqDistance(loc1.getX(), loc2.getX(), loc1.getY(), loc2.getY(), loc1.getZ(), loc2.getZ()) < Math.pow(radius, 2);
    }

    public static Location newLocation(Location template) {
        return new Location(template.getWorld(), template.getX(), template.getY(), template.getZ(), template.getYaw(), template.getPitch());
    }
}
