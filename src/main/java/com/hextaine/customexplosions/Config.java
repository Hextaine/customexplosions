package com.hextaine.customexplosions;
/*
 * Created by Hextaine on 5/8/2021.
 * Email: hextaine@gmail.com
 * Discord: Hextaine#2719
 * Enjoy!
 */

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Config {
    private CustomExplosions plugin;
    private String resource = null;
    private FileConfiguration config = null;
    private File configFile = null;

    public Config(CustomExplosions plugin, String resource) {
        // Constructor
        this.plugin = plugin;
        this.resource = resource;
        this.saveDefaultConfig();
    }

    public void reload() {
        if (this.configFile == null) {
            this.configFile = new File(this.plugin.getDataFolder(), this.resource);
        }
        this.config = YamlConfiguration.loadConfiguration(this.configFile);

        InputStream templateFile = this.plugin.getResource(this.resource);
        if (templateFile != null) {
            YamlConfiguration templateConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(templateFile));
            this.config.setDefaults(templateConfig);
        }
    }

    // Returns config file
    public FileConfiguration getConfig() {
        if (this.config == null) {
            this.reload();
        }
        return this.config;
    }

    // Save config if it already exists, otherwise do nothing. If save fails, log to console.
    public void saveConfig() {
        if (this.config == null || this.configFile == null) {
            return; // Cannot save anything because file doesn't exist
        }
        try {
            this.config.save(this.configFile);
        } catch (Exception e) {
            this.plugin.getLogger().severe("failed to save config file to " + this.configFile.getAbsolutePath());
        }
    }

    // Loads the default config file we have saved in plugin resources directory. If a file already exists,
    // do nothing.
    public void saveDefaultConfig() {
        if (this.configFile == null) {
            this.configFile = new File(this.plugin.getDataFolder(), this.resource);
        }

        if (!this.configFile.exists()) {
            // Save the config from our plugin resources, but don't overwrite anything
            this.plugin.saveResource(this.resource, false);
        }
    }
}
